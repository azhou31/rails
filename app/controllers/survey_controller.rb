class SurveyController < ApplicationController

def index
    session[:views] = 0
end

def submit
    session[:views] += 1
    session[:result] = params[:survey]
    redirect_to "/survey/result"
end

def result
    @result=session[:result]
end

end
