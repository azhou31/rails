Rails.application.routes.draw do
get "survey" => "survey#index"
post "survey" => "survey#submit"
get "survey/result" => "survey#result"
end
